<?php include('includes/init.php');?>
<!DOCTYPE html>
<html lang="en">

<?php include('includes/header2.php'); ?>

<body>

<div class = "firstrow">

  <div class = "box2">
    <figure>
      <!--image from https://en.wikipedia.org/wiki/Citigroup-->
      <img src = "images/citi.png" alt = "pokeball symbol" width = 70/>
    </figure>
  </div>

  <div class = "box1">
    <h1>Investivia</h1>
  </div>
</div>

<hr/>
<div class = "topnav">
  <nav>
    <ul>
      <li class="<?php echo ($title == 'citi') ? 'current_page' : '' ?>"><a href="citi.php">Home</a></li>
      <li class="<?php echo ($title == 'videos') ? 'current_page' : '' ?>"><a href="videos.php">Lessons</a></li>
      <li class="<?php echo ($title == 'dictionary') ? 'current_page' : '' ?>"><a href="dictionary.php">Dictionary</a></li>
      <li class="<?php echo ($title == 'my_money') ? 'current_page' : '' ?>"><a href="portfolio.php">My Money</a></li>
    </ul>
  </nav>
</div>
<hr/>

  <main>

  <form id="uploadFile" action="quiz.php" form method = "POST" enctype = "multipart/form-data">

  <div class="group_label_input">
    <label for = "type1">Question 1:</label>
    <select id = "type1" name="type1">
      <option value="" selected disabled>Options: </option>
    </select>
    <div class = "errors">
    <?php
    if (isset($_POST["submit_upload"])) {
      if ($tag_name == ""){
      echo $tag_name_error;
      }}
    ?>
    </div>
  </div>


<button name="submit_upload" type="submit">Submit Questionaire</button>
</div>
</form>

  </main>
</body>

</html>
