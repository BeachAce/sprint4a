<header>
  <title>Pokemon</title>
</header>

<div class = "firstrow">
  <div class = "box1">
    <h1>Pokemon</h1>
  </div>

  <div class = "box2">
    <figure>
      <!--image from https://en.wikipedia.org/wiki/File:Pokeball.PNG-->
      <img src = "images/Pokeball.png" alt = "pokeball symbol" width = 70/>
        <figcaption><cite><a href="https://en.wikipedia.org/wiki/File:Pokeball.PNG">https://en.wikipedia.org/wiki/File:Pokeball.PNG</a></cite>
        </figcaption>
    </figure>
  </div>
</div>

<hr/>

<div class="topnav">

<nav>
      <ul>
        <li><a href="index.php" class = <?php echo $Home ?>>Home</a></li>
        <li><a href="add.php" class = <?php echo $Add ?>>Add a Pokemon</a></li>
        <li><a href="types.php" class = <?php echo $Add ?>>Pokemon Types</a></li>
      </ul>
</nav>
</div>

<hr/>
