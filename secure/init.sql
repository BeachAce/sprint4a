BEGIN TRANSACTION;

CREATE TABLE dictionary1 (
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	term_name TEXT NOT NULL,
	term_description TEXT NOT NULL
);

INSERT INTO dictionary1 (id, term_name, term_description) VALUES (1,'Asset Allocation', 'This is just a fancy phrase for your investment strategy. There are three general categories where you''re going to put your money: cash, bonds and stocks, says Kemberley Washington, an accounting professor at Dillard University in New Orleans and a certified public accountant. ');

INSERT INTO dictionary1 (id, term_name, term_description) VALUES (2,'Cash', 'Since Washington brought it up, let''s define cash. It''s money – you know that – but if a financial advisor suggests you move some of your portfolio into cash, Washington says he or she is probably referring to certificates of deposit, also known as CDs, Treasury bills or money market accounts.');

INSERT INTO dictionary1 (id, term_name, term_description) VALUES (3,'Bonds', 'When you invest in a bond, you are essentially loaning money to a company or government. Provided that nothing bad happens, like a bankruptcy, you cash in the bond on the maturity date and collect some interest.');

INSERT INTO dictionary1 (id, term_name, term_description) VALUES (4,'Stocks', 'When you buy stock in a company, you''re purchasing a tiny bit of ownership in the firm. Generally, the better the company performs, the more your share of stock is worth. If the company doesn''t do so well, your stock may be worth less.');

INSERT INTO dictionary1 (id, term_name, term_description) VALUES (5,'Mutual Fund', 'In layman''s terms, this is a pile of money that comes from a lot of investors like you and is then invested in assets like stocks and bonds. A mutual fund may hold hundreds of stocks, with the purpose of spreading the risk. In most cases, money managers make buy and sell decisions for mutual funds, which brings us to our next definition.');

INSERT INTO dictionary1 (id, term_name, term_description) VALUES (6,'Expense Ratio', 'It costs money to run mutual funds, so investors can expect to pay an annual fee, expressed as the expense ratio. "That''s the percentage of your money that goes to the managers of the mutual fund you''re investing in," says Keith Singer, a certified financial planner with a wealth management firm in Boca Raton, Florida. The expense ratio also covers other fund expenses, such as administrative fees, record-keeping fees and even print or TV ads promoting the mutual fund. In 2013, the average stock mutual fund had an expense ratio of 1.25 percent, according to Morningstar.');

INSERT INTO dictionary1 (id, term_name, term_description) VALUES (7,'Index Funds', 'This is a popular type of mutual fund because its costs are generally low – think more like 0.2 percent. But if you really want to understand index funds, you first need to understand indexes, which are essentially collections of stocks that represent a slice of the');

INSERT INTO dictionary1 (id, term_name, term_description) VALUES (8,'Target-date Funds', 'Often found in 401(k) plans, target-date funds are designed to serve as all-in-one portfolios that are tailored to your expected retirement date. So if you have about 30 years until retirement, you might invest in a 2045 target-date fund. In the beginning, your investments will be riskier and more heavily weighted toward stocks, then as you get closer to 2045, the investments will become increasingly more conservative and will shift to include more bonds.');

INSERT INTO dictionary1 (id, term_name, term_description) VALUES (9,'Price-to-earnings Ratio', 'Remember in math class when you learned that a ratio is a relationship between two numbers? Here, you''re looking at a company’s stock price in relation to its earnings. "The price-earnings ratio gives you a general measure of whether your investments are overvalued or not," Singer says.');

INSERT INTO dictionary1 (id, term_name, term_description) VALUES (10,'Prospectus', 'Looking for a go-to source that contains every bit of information about an investment? Ask your financial advisor for a prospectus, or search online to find one. It''s a legal document that contains in-depth details about stocks, bonds, a mutual fund or whatever you''re planning to invest in. If you''re wondering, for instance, what the expense ratio is on your mutual fund, or you would like a list of all the fund''s holdings, you''d find it in the prospectus.
');

CREATE TABLE pokemon (
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	pokemon_name TEXT NOT NULL UNIQUE,
	region TEXT NOT NULL,
	tier TEXT NOT NULL
);

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (1, 'Lapras', 'Kanto', 'Regular');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (2, 'Chikorita', 'Johto', 'Regular');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (3, 'Moltres', 'Kanto', 'Legendary');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (4, 'Salamence', 'Hoenn', 'Legendary');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (5, 'Magikarp', 'Kanto', 'Regular');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (6, 'Wobbuffet', 'Johto', 'Regular');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (7, 'Raikou', 'Johto', 'Legendary');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (8, 'Milotic', 'Hoenn', 'Regular');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (9, 'Shinx', 'Sinnoh', 'Regular');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (10, 'Croagunk', 'Sinnoh', 'Regular');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (11, 'Latias', 'Hoenn', 'Legendary');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (12, 'Clefable', 'Kanto', 'Regular');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (13, 'Metagross', 'Hoenn', 'Legendary');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (14, 'Gastly', 'Kanto', 'Regular');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (15, 'Beautifly', 'Hoenn', 'Regular');

INSERT INTO pokemon (id, pokemon_name, region, tier) VALUES (16, 'Umbreon', 'Johto', 'Regular');




CREATE TABLE tags (
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	types TEXT NOT NULL,
	type_info TEXT NOT NULL,
	type_source TEXT
);

INSERT INTO tags (id, types, type_info, type_source) VALUES (1, 'Bug', 'Most Bug Pokémon grow quickly and evolve sooner than other types. As a result, they are often very weak. In Generation I, bugs were almost useless since the few Bug type moves available were very weak. The situation improved in later games with better moves and an advantage against the Dark type.', 'https://pokemondb.net/type/bug');

INSERT INTO tags (id, types, type_info, type_source) VALUES (2, 'Dark', 'The Dark type was introduced in the second generation of Pokémon games as a measure to balance the types. In particular, its resistance to Psychic cut down that type''s advantage by a long way. When paired with the Ghost type it was the only type combination to have no weaknesses prior to Gen 6.', 'https://pokemondb.net/type/dark');

INSERT INTO tags (id, types, type_info, type_source) VALUES (3, 'Dragon', 'Dragons are among the most elusive and powerful of all Pokémon. Nine legendary Pokémon are part Dragon type and four have legendary-like stats.They are notoriously difficult to train due to requiring more EXP points per level than most non-legendary Pokémon, and the fact they evolve much later and thus are in their weaker forms for longer. Interestingly, many final-evolution Dragon types have a 4x weakness to the Ice type.', 'https://pokemondb.net/type/dragon');

INSERT INTO tags (id, types, type_info, type_source) VALUES (4, 'Electric', 'There are relatively few Electric Pokémon; in fact only four were added in the third generation. Most are based on rodents or inanimate objects. Electric Pokémon are very good defensively, being weak only to Ground moves. Eelektross is the only Pokémon to have no type disadvantages due to its ability, Levitate.', 'https://pokemondb.net/type/electric');

INSERT INTO tags (id, types, type_info, type_source) VALUES (5, 'Fairy', 'The Fairy type was introduced in Generation 6 - the first new type for more than 12 years! Its main intention was to balance the type chart by reducing the power of dragons, while also giving an offensive boost to the Poison and Steel types. Several old Pokémon were retyped and new Pokémon introduced. In total there are just 34 Fairy type Pokémon (not including Megas/Formes), slightly above Ice.', 'https://pokemondb.net/type/fairy');

INSERT INTO tags (id, types, type_info, type_source) VALUES (6, 'Fighting', 'Fighting Pokémon are strong and muscle-bound, often based on martial artists. Fighting moves are super-effective against five other types (as is Ground), making them very good offensively. Most Fighting type moves are in the Physical category, for obvious reasons.', 'https://pokemondb.net/type/fighting');

INSERT INTO tags (id, types, type_info, type_source) VALUES (7, 'Fire', 'Fire is one of the three basic elemental types along with Water and Grass, which constitute the three starter Pokémon. This creates a simple triangle to explain the type concept easily to new players. Fire types are notoriously rare in the early stages of the games so choosing the Fire variation starter is often a plus.', 'https://pokemondb.net/type/fire');

INSERT INTO tags (id, types, type_info, type_source) VALUES (8, 'Flying', 'Most Flying type Pokémon are based on birds or insects, along with some mythical creatures like dragons. On average they are faster than any other type. Nearly every Flying type has Flying as the secondary type, usually with Normal. There is only one pure Flying Pokémon (Tornadus), and one line with Flying as a primary type (Noibat/Noivern). As of Generation 6, the type has also been paired with every other type.', 'https://pokemondb.net/type/flying');

INSERT INTO tags (id, types, type_info, type_source) VALUES (9, 'Ghost', 'Ghosts are rare Pokémon, and the only type to have two immunities. In total there are just 34 Ghost type Pokémon (not including Megas/Formes), slightly above Ice. In the first generation, Ghost moves has no effect on Psychic Pokémon, however, it was later changed to be super-effective. When paired with the Dark type it was the only type combination to have no weaknesses prior to Gen 6.', 'https://pokemondb.net/type/ghost');

INSERT INTO tags (id, types, type_info, type_source) VALUES (10, 'Grass', 'Grass is one of the three basic elemental types along with Fire and Water, which constitute the three starter Pokémon. This creates a simple triangle to explain the type concept easily to new players. Grass is one of the weakest types statistically, with 5 defensive weaknesses and 7 types that are resistant to Grass moves. Furthermore, many Grass Pokémon have Poison as their secondary type, adding a Psychic vulnerability. The type combination with the most weaknesses is Grass/Psychic.', 'https://pokemondb.net/type/grass');

INSERT INTO tags (id, types, type_info, type_source) VALUES (11, 'Ground', 'Ground is one of the strongest types offensively: it is super-effective against five other types (as is Fighting) and Earthquake is one of the strongest moves in the game with power and accuracy both 100. Unfortunately, many Ground type Pokémon are dual Rock types, lumbering them with 4x Grass and Water disadvantages.', 'https://pokemondb.net/type/ground');

INSERT INTO tags (id, types, type_info, type_source) VALUES (12, 'Ice', 'Ice type Pokémon are now the rarest of all types: there are just 33 in total (ignoring Megas/Formes). They are ranked quite well defensively in terms of stats, although multiple type weaknesses let them down. Some are based on typical Arctic creatures like seals or yaks, while others are more mythical.', 'https://pokemondb.net/type/ice');

INSERT INTO tags (id, types, type_info, type_source) VALUES (13, 'Normal', 'The Normal type is the most basic type of Pokémon. They are very common and appear from the very first route you visit. Most Normal Pokémon are single type, but there is a large contingent having a second type of Flying. Pokémon X/Y add several Normal dual-type Pokémon.', 'https://pokemondb.net/type/normal');

INSERT INTO tags (id, types, type_info, type_source) VALUES (14, 'Poison', 'The Poison type is regarded as one of the weakest offensively. Prior to Pokémon X/Y it was super-effective only against Grass (many of which are dual Poison so neutralizes the effect). It now has an extra advantage against the new Fairy type. In the first generation it was also super-effective against Bug but this was changed. It fares a little better defensively but its best advantage is through status moves like Toxic.', 'https://pokemondb.net/type/poison');

INSERT INTO tags (id, types, type_info, type_source) VALUES (15, 'Psychic', 'The Psychic type has few outright strengths, however, it also has few weaknesses. In the first generation it ended up being massively overpowered, mainly due to a complete lack of powerful Bug moves, its only weakness. Furthermore, a mistake in the game meant that Ghost-type moves had no effect on Psychic (although this only affected the low-powered Lick). Generation 2 rectified the situation with the addition of the Dark type along with better Pokémon and moves of all types.', 'https://pokemondb.net/type/psychic');

INSERT INTO tags (id, types, type_info, type_source) VALUES (16, 'Rock', 'Rock is a solid type as one might expect. Like Steel, Rock Pokémon usually have high defense - however, since many Rock Pokémon are part Ground they have a 4x weakness to both Grass and Water whose moves often come as Special type.', 'https://pokemondb.net/type/rock');

INSERT INTO tags (id, types, type_info, type_source) VALUES (17, 'Steel', 'The Steel type was introduced in the second generation of Pokémon games. It is the strongest type defensively, with 10 types being not very effective against it and the Poison type having no effect. From Pokémon X/Y onwards, it lost its Ghost and Dark resistance, those types now dealing neutral damage. The Steel type also has the highest average Defense stat in the games.', 'https://pokemondb.net/type/steel');

INSERT INTO tags (id, types, type_info, type_source) VALUES (18, 'Water', 'Water is one of the three basic elemental types along with Fire and Grass, which constitute the three starter Pokémon. This creates a simple triangle to explain the type concept easily to new players. Water is the most common type with over 100 Pokémon, which are based on a wide variety of fish and other sea-dwelling creatures. As of Generation 6, Water has been paired with every other type.', 'https://pokemondb.net/type/water');

CREATE TABLE tag_weakness (
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	tag_id INTEGER NOT NULL,
	weak INTEGER NOT NULL
);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (1, 1, 7);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (2, 1, 8);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (3, 1, 16);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (4, 2, 1);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (5, 2, 5);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (6, 2, 6);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (7, 3, 3);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (8, 3, 5);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (9, 3, 12);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (10, 4, 11);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (11, 5, 14);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (12, 5, 17);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (13, 6, 5);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (14, 6, 8);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (15, 6, 15);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (16, 7, 11);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (17, 7, 16);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (18, 7, 18);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (19, 8, 4);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (20, 8, 12);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (21, 8, 16);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (22, 9, 2);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (23, 9, 9);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (24, 10, 1);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (25, 10, 7);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (26, 10, 8);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (27, 10, 12);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (28, 10, 14);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (29, 11, 10);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (30, 11, 12);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (31, 11, 18);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (32, 12, 6);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (33, 12, 7);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (34, 12, 16);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (35, 12, 17);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (36, 13, 6);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (37, 14, 11);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (38, 14, 15);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (39, 15, 1);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (40, 15, 2);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (41, 15, 9);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (42, 16, 6);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (43, 16, 10);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (44, 16, 11);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (45, 16, 17);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (46, 16, 18);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (47, 17, 6);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (48, 17, 7);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (49, 17, 11);

INSERT INTO tag_weakness (id, tag_id, weak) VALUES (50, 18, 4);
INSERT INTO tag_weakness (id, tag_id, weak) VALUES (51, 18, 10);


-- images table
CREATE TABLE images (
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	users_name TEXT NOT NULL,
	image_name TEXT NOT NULL,
	image_ext TEXT NOT NULL,
	description TEXT,
	source TEXT
);

-- documents seed data
INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (1, 'Camryn', 'lapras.png', 'png', 'People have driven Lapras almost to the point of extinction. In the evenings, this Pokémon is said to sing plaintively as it seeks what few others of its kind still remain.', 'https://www.pokemon.com/us/pokedex/lapras');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (2, 'Camryn', 'chikorita.png', 'png', 'In battle, Chikorita waves its leaf around to keep the foe at bay. However, a sweet fragrance also wafts from the leaf, becalming the battling Pokémon and creating a cozy, friendly atmosphere all around.', 'https://www.pokemon.com/us/pokedex/chikorita');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (3, 'Camryn', 'moltres.png', 'png', 'Moltres is a legendary bird Pokémon that has the ability to control fire. If this Pokémon is injured, it is said to dip its body in the molten magma of a volcano to burn and heal itself.', 'https://www.pokemon.com/us/pokedex/moltres');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (4, 'Camryn', 'salamence.png', 'png', 'Salamence came about as a result of a strong, long-held dream of growing wings. It is said that this powerful desire triggered a sudden mutation in this Pokémon''s cells, causing it to sprout its magnificent wings.', 'https://www.pokemon.com/us/pokedex/salamence');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (5, 'Camryn', 'magikarp.png', 'png', 'Magikarp is a pathetic excuse for a Pokémon that is only capable of flopping and splashing. This behavior prompted scientists to undertake research into it.', 'https://www.pokemon.com/us/pokedex/magikarp');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (6, 'Camryn', 'wobbuffet.png', 'png', 'If two or more Wobbuffet meet, they will turn competitive and try to outdo each other''s endurance. However, they may try to see which one can endure the longest without food. Trainers need to beware of this habit.', 'https://www.pokemon.com/us/pokedex/wobuffet');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (7, 'Camryn', 'raikou.png', 'png', 'Raikou embodies the speed of lightning. The roars of this Pokémon send shock waves shuddering through the air and shake the ground as if lightning bolts had come crashing down.', 'https://www.pokemon.com/us/pokedex/raikou');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (8, 'Camryn', 'milotic.png', 'png', '
Milotic is said to be the most beautiful of all the Pokémon. It has the power to becalm such emotions as anger and hostility to quell bitter feuding.', 'https://www.pokemon.com/us/pokedex/milotic');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (9, 'Camryn', 'shinx.png', 'png', 'The extension and contraction of its muscles generates electricity. It glows when in trouble.', 'https://www.pokemon.com/us/pokedex/shinx');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (10, 'Camryn', 'croagunk.png', 'png', 'Its cheeks hold poison sacs. It tries to catch foes off guard to jab them with toxic fingers.', 'https://www.pokemon.com/us/pokedex/croagunk');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (11, 'Camryn', 'latias.png', 'png', 'Latias is highly sensitive to the emotions of people. If it senses any hostility, this Pokémon ruffles the feathers all over its body and cries shrilly to intimidate the foe.', 'https://www.pokemon.com/us/pokedex/latias');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (12, 'Camryn', 'clefable.png', 'png', 'Clefable moves by skipping lightly as if it were flying using its wings. Its bouncy step lets it even walk on water. It is known to take strolls on lakes on quiet, moonlit nights.', 'https://www.pokemon.com/us/pokedex/clefable');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (13, 'Camryn', 'metagross.png', 'png', 'Metagross has four brains in total. Combined, the four brains can breeze through difficult calculations faster than a supercomputer. This Pokémon can float in the air by tucking in its four legs.', 'https://www.pokemon.com/us/pokedex/metagross');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (14, 'Camryn', 'gastly.png', 'png', 'Gastly is largely composed of gaseous matter. When exposed to a strong wind, the gaseous body quickly dwindles away. Groups of this Pokémon cluster under the eaves of houses to escape the ravages of wind.', 'https://www.pokemon.com/us/pokedex/gastly');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (15, 'Camryn','beautifly.png', 'png', 'Beautifly''s favorite food is the sweet pollen of flowers. If you want to see this Pokémon, just leave a potted flower by an open window. Beautifly is sure to come looking for pollen.', 'https://www.pokemon.com/us/pokedex/beautifly');

INSERT INTO images (id, users_name, image_name, image_ext, description, source) VALUES (16, 'Camryn','umbreon.png', 'png', 'Umbreon evolved as a result of exposure to the moon''s waves. It hides silently in darkness and waits for its foes to make a move. The rings on its body glow when it leaps to attack.', 'https://www.pokemon.com/us/pokedex/umbreon');



CREATE TABLE image_tags (
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	pokemon_id INTEGER NOT NULL,
	tag_id INTEGER NOT NULL
);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (1, 1, 18);
INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (2,1,12);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (3, 2, 10);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (4, 3, 7);
INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (5,3,8);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (6, 4, 3);
INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (7,4,8);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (8, 5, 18);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (9,6,15);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (10, 7, 4);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (11, 8, 18);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (12,9,4);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (13, 10, 6);
INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (14,10,14);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (15, 11, 3);
INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (16,11,15);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (17, 12, 5);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (18, 13, 15);
INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (19, 13,17);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (20, 14, 9);
INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (21,14,14);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (22, 15, 1);
INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (23,15,8);

INSERT INTO image_tags (id, pokemon_id, tag_id) VALUES (24, 16, 2);




COMMIT;
