# Project 3: Design Journey

Be clear and concise in your writing. Bullets points are encouraged.

**Everything, including images, must be visible in VS Code's Markdown Preview.** If it's not visible in Markdown Preview, then we won't grade it.

# Design & Plan (Milestone 1)

## Describe your Gallery (Milestone 1)
> What will your gallery be about? 1 sentence.

My galelry will be an imahe gallery about different types of Pokemon.

> Will you be using your existing Project 1 or Project 2 site for this project? If yes, which project?

Yes, I will be using my existing site from Project 2 for this project.

> If using your existing Project 1 or Project 2, please upload sketches of your final design here.

This is the final design sketch for my Project 2 (I have alterned this significantly and have included those sketches in the design process section below)
![](finalsketch.jpg)


## Target Audience(s) (Milestone 1)
> Tell us about your target audience(s).

My target audience is people looking to study Pokemon, discover different types of pokemon, enjoy collecting pokemon, learn things about specific types of Pokemon, or just love looking at Pokemon facts. My site is aimed towards people who are somewhat familiar with Pokemon, but is also useable to people who are unfamiliar with the terminology since I made it very clear how Pokemon can be classified. Ive included brief descriptions/instructions on my 3 main pages that are tabs in my nav bar, so someone who has studied Pokemon before will be able to use the site to learn. In general, my target audience will be people that have played the games or watched the show, so mainly kids and young adults. In essence my site is mainly aimed towards begginers to intermediate players, I do not have advanced facts displayed on my website such as possible attacks and defenses for each type of pokemon. However, my site is very useful when looking up basic important facts about pokemon so this would appeal to the amateur player.


## Design Process (Milestone 1)
> Document your design process. Show us the evolution of your design from your first idea (sketch) to design you wish to implement (sketch). Show us the process you used to organize content and plan the navigation (card sorting), if applicable.
> Label all images. All labels must be visible in VS Code's Markdown Preview.
> Clearly label the final design.

Initial sketch
![](first.jpg)

I decided to split my website into two different pages, one for the collection of pokemon, and one into a page where users can add pokemon.

Draft #2 sketches:
(Home page)
![](final1.jpg)
(Add a pokemon page)
![](final2.jpg)

I decided to add an additional page to display all of my tags (pokemon types) at once. This new "Pokemon Types" page displays every tag type and a brief description.  I also did a sketch of my image details page (page with one enlarged image and all tags) there was quite a bit of designing for this so I decided to sketch it first.

Draft #3 sketches (final sketches):

(Home page final)
![](homefinal.jpg)

(Add a pokemon page final)
![](addfinal.jpg)

(Types page final)
![](types.jpg)

(Image details page final)
![](image_details.jpg)


## Design Patterns (Milestone 1)
> Explain how your site leverages existing design patterns for image galleries.
> Identify the parts of your design that leverage existing design patterns and justify their usage.
> Most of your site should leverage existing patterns. If not, fully explain why your design is a special case (you need to have a very good reason here to receive full credit).

My site design cleanly displays an entries image using up horizontal and vertical space. I used flex wrapping so that the home page will use horizontal space by putting in as many entries as possible depending on the size of the users computer page. My layout is very similar to how other popular websites display thier image galleries such as instagram and pinterest. A user can scroll through all of my entries on the home page, and then when clicked the user can see the enlarged image along with all of its details. Also similar to many existing image gallery sites, I have my search bar positioned above all of my entries and users can use this to search through my entries. I have also included a serch by category feature so that users can further refine thier seraches based on what they're looking for. Users can either search from everythig (they can search across names, types, regions, tiers) or they can select a category (names, types regions, or tiers) and thier search will only search for things in those categories. I have designed my site to be easy to use, and implemeting my search this way allows y users to easily find what they are looking to find. In addition, my site is organized as neatly as possible, and the lines I've used in my design are aesthetically pleasing to the eye, further making my site more enjpyable to use. Similarly to many popular image galleries I've implemented a very simple, yet clean appearance that appeals to the average person.

## Requests (Milestone 1)
> Identify and plan each request you will support in your design.
> List each request that you will need (e.g. view image details, view gallery, etc.)
> For each request, specify the request type (GET or POST), how you will initiate the request: (form or query string param URL), and the HTTP parameters necessary for the request.

Example:
- Request: Search Pokemon on home page
  - Type: GET
  - Params: (':search'=> $search)

-  Request: go back to home page
    - Type: GET


- Request: submit add pokemon form
  - Type: POST
  - Params: ( ':basename' => $basenamae, ':image_name'=>$image_name, ':users_name' =>$users_name, ':image_ext' => $image_ext, ':box_desc'=> $box_desc, ':pokemon_name' => $pokemon_name, ':tier' = > $tier, ':region' => $region )

- Request: delete pokemon
    - Type: GET
    - Params: (':pokemon_number' => $pokemon_number)

- Request: add tag to pokemon
  - Type: GET
  - Params: (':pokemon_number' => $pokemon_number, ':type_add' => $type_add)

- Request: remove tag from pokemon
  - Type: GET
  - Params: (':pokemon_number' => $pokemon_number, ':type_remove' => $type_remove)

- Request: delete pokemon
  - Type: GET
  - Params: (':pokemon_number' => $pokemon_number)

- Request: next pokemon
  - Type: POST
  - Params: (':image' => $image)

- Request: delete pokemon
  - Type: POST
  - Params: (':image' => $image)

## Database Schema Design (Milestone 1)
> Plan the structure of your database. You may use words or a picture.
> Make sure you include constraints for each field.

> Hint: You probably need `images`, `tags`, and `image_tags` tables.

> Hint: For foreign keys, use the singular name of the table + _id. For example: `image_id` and `tag_id` for the `image_tags` table.


pokemon (
  id : INTEGER {PK, U, Not, AI},
  pokemon_name: TEXT {U, Not},
  region: TEXT {Not},
  tier: TEXT {Not},
  description: TEXT {Not},
  source: TEXT
)
tags (
  id : INTEGER {PK, U, Not, AI},
  types : TEXT {Not},
  type_info: TEXT {Not},
  type_source: TEXT
)

tag_weakness (
  id : INTEGER {PK, U, Not, AI},
  tag_id : INTEGER {Not},
  weak: INTEGER {Not}
)

images (
  id : INTEGER {PK, U, Not, AI},
  users_name : TEXT {Not},
  image_name: TEXT {Not},
  image_ext: TEXT {Not}
)

image_tags (
  id: INTEGER {PK, U, Not, AI},
  pokemon_id: INTEGER {Not},
  tag_id: INTEGER {Not}
)


## Database Query Plan (Milestone 1)
> Plan your database queries. You may use natural language, pseudocode, or SQL.
> Using your request plan above, plan all of the queries you need.

For searching my home page across all fields:
"SELECT DISTINCT pokemon.id, pokemon.pokemon_name, pokemon.region, pokemon.tier, images.id, images.image_name, images.image_ext, images.description FROM pokemon INNER JOIN images ON images.id = pokemon.id INNER JOIN image_tags ON image_tags.pokemon_id = pokemon.id INNER JOIN tags ON image_tags.tag_id = tags.id WHERE pokemon.pokemon_name LIKE '%' || :search || '%' OR tags.types LIKE '%' || :search || '%' OR pokemon.region LIKE '%' || :search || '%' OR pokemon.tier LIKE '%' || :search || '%';"

For searching across names:
"SELECT pokemon.id, pokemon.pokemon_name, pokemon.region, pokemon.tier, images.id, images.image_name, images.image_ext, images.description FROM pokemon INNER JOIN images ON images.id = pokemon.id WHERE pokemon.pokemon_name LIKE '%' || :search || '%' ;"

For serach across tags (types):
"SELECT pokemon.id, pokemon.pokemon_name, pokemon.region, pokemon.tier, images.id, images.image_name, images.image_ext, images.description FROM pokemon INNER JOIN images ON images.id = pokemon.id INNER JOIN image_tags ON image_tags.pokemon_id = pokemon.id INNER JOIN tags ON image_tags.tag_id = tags.id WHERE tags.types LIKE '%' || :search || '%' ;"

For searching across regions:
"SELECT pokemon.id, pokemon.pokemon_name, pokemon.region, pokemon.tier, images.id, images.image_name, images.image_ext, images.description FROM pokemon INNER JOIN images ON images.id = pokemon.id WHERE pokemon.region LIKE '%' || :search || '%' ;"

For searching across tiers:
"SELECT pokemon.id, pokemon.pokemon_name, pokemon.region, pokemon.tier, images.id, images.image_name, images.image_ext, images.description FROM pokemon INNER JOIN images ON images.id = pokemon.id WHERE pokemon.tier LIKE '%' || :search || '%' ;"

For displaying all images on my home page:
"SELECT pokemon.id, pokemon.pokemon_name, pokemon.region, pokemon.tier, images.id, images.image_name, images.image_ext, images.description FROM pokemon INNER JOIN images ON images.id = pokemon.id"

For when a user adds pokemon (inserts into database user's name, pokemon name, types, region, tier, image file, description, and source):

"INSERT INTO images (users_name, image_name, image_ext, description) VALUES (:users_name, :basename, :upload_ext, :description)"
"INSERT INTO pokemon (pokemon_name, region, tier) VALUES (:pokemon_name, :region, :tier)"
"SELECT id FROM tags WHERE types = :tag_name"
"INSERT INTO image_tags (pokemon_id, tag_id) VALUES (:pokemon_id, :tag_id)"
"SELECT id FROM tags WHERE types = :tag_name2"
"INSERT INTO image_tags (pokemon_id, tag_id) VALUES (:pokemon_id, :tag_id2)"

For when a user views a single image and its details:
"SELECT pokemon.id, pokemon.pokemon_name, pokemon.region, pokemon.tier, images.id, images.image_name, images.image_ext, images.description FROM pokemon INNER JOIN images ON images.id = pokemon.id WHERE pokemon.id = $image1"
"SELECT tags.types FROM tags INNER JOIN image_tags ON tags.id = image_tags.tag_id WHERE image_tags.pokemon_id = $current_index2"

For getting all tags for types page:
"SELECT types, type_info, type_source FROM tags"


## Code Planning (Milestone 1)
> Plan what top level PHP pages you'll need.

I will need a page for when I click on a pokemon (the image details page).
I will also need one for when I want to delete a pokemon, add a tag, or remove a tag. (user will be directed to this page when they are on the image details page and select an option)


> Plan what partials you'll need.

I will be using partials for my navigation bar at the top of my page.


> Plan any PHP code you'll need.

Ill need php to declare variables, arrays open init, link my database, and write my 2 user defined functions. Ill also need php to filter, sanatise, and escape my variable to protect users privacy. Ill use php to write if statements to search my database depending on field. Ill also be using php to echo confirmation and error messages.


# Complete & Polished Website (Final Submission)

## Gallery Step-by-Step Instructions (Final Submission)
> Write step-by-step instructions for the graders.
> For each set of instructions, assume the grader is starting from index.php.

Viewing all images in your gallery:
1. If you open index.php (the home page) every image in my gallery is displayed here.
2. Scroll downto see every image.

View all images for a tag:
1. Open index.php (the home page) and enter into the serachbar the tag you wish to search. You can either search everything (names, tags, etc...) or you can specifically search by tag. For my website tags are pokemon types.
2. Once you type the tag into the searchbar and click search, the images that appear will be the ones that have that tag.

View a single image and all the tags for that image:
1. Open index.php (the home page) and click on any image of a pokemon.
2. Once you click on an image, you will then be taken to a page with just that pokemon's image and all of its details. You will see its name, its type or types, types its weak agaisnt, and a description.

How to upload a new image:
1. From the index.php page (the home page) click on the "Add a Pokemon" tab in the nav bar. This will take you to the add.php page where there will be a form you can add pokemon.
2. Fill out the form on this page and click "Add this Pokemon" if the form was filled out correctly, your pokemon will be added and you can view it in the image gallery.

How to delete an image:
1. From the index.php page (the home page) click on the photo of the pokemon image you would like to delete. This will take you to view the single image and its details.
2. Next, click on the image options button and then select "Delete pokemon" and this pokemon will be deleted.

How to view all tags at once:
1. From the index.php page (the home page) click the "Pokemon Types" tab and this will take you to types.php.
2. Here you can view all currents tags (pokemon types) at once and there is a brief description for each.

How to add a tag to an existing image:
1. From the index.php page (the home page) click on the photo of the pokemon image you would like to add a tag for. This will take you to view the single image and its details.
2. Next, click on image options and select "Add tag". This will then ask you what tag you would like to add.

How to remove a tag from an existing image:
1. From the index.php page (the home page) click on the photo of the pokemon image you would like to remove a tag for. This will take you to view the single image and its details.
2. Next, click the image options and select "Remove tag". This will then ask you which tag to remove.


## Reflection (Final Submission)
> Take this time to reflect on what you learned during this assignment. How have you improved since starting this class?

From this assignment, I have learned so much how to figure out things on my own. Usually for these projects I'm in office hours for this class several times a week, but due to the situation, I've had to figure out much of this project on my own. I was determined to do the best quality work I could do, I had no doubt in my mind I would be able to do good work because I devoted so much of my time to this. I had trouble finishing on time, it was difficult trying to figure out things on my own and it just took a lot of time. However, the time was well worth it, through struggling through the tasks of this assignment I find that I genuinely understand my code better. I understand whats going on in my code and I've learned to debug my own code much more effectively. Using php to var_dump and echo certain variables helped me figure out almost all the issues I faced during this assignmnet. Without office hours, I was forced to figure out alot of the problems I had by myself, and in turn this helped me become more reliant on my own programing skills. I have definitly improved tremendously since starting this class, I understand more how to think like a programer, in particular I've learned how to problem solve on my own to a much greater extent.
