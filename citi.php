<?php include('includes/init.php');?>
<!DOCTYPE html>
<html lang="en">

<?php include('includes/header2.php'); ?>

<body>

<div class = "firstrow">

  <div class = "box2">
    <figure>
      <!--image from https://en.wikipedia.org/wiki/Citigroup-->
      <img src = "images/citi.png" alt = "pokeball symbol" width = 70/>
    </figure>
  </div>

  <div class = "box1">
    <h1>Investivia</h1>
  </div>
</div>

<hr/>
<div class = "topnav">
  <nav>
    <ul>
    <li class="<?php echo ($title == 'citi') ? 'current_page' : '' ?>"><a href="citi.php">Home</a></li>
      <li class="<?php echo ($title == 'videos') ? 'current_page' : '' ?>"><a href="videos.php">Lessons</a></li>
      <li class="<?php echo ($title == 'dictionary') ? 'current_page' : '' ?>"><a href="dictionary.php">Dictionary</a></li>
      <li class="<?php echo ($title == 'my_money') ? 'current_page' : '' ?>"><a href="portfolio.php">My Money</a></li>
    </ul>
  </nav>
</div>
<hr/>

  <main>

  <div class = "mainone">

<div class = "leftone">
<h2>Welcome!</h2>
  <p>Investivia is Citibank's free online educational app for those who are new to investing. Our platform gives you the knowledge and skill you need to make informed investment decisions and learn what plan works best for you. Learn what blah blah blah blah ....</p>

  <h2>Contact Us</h2>
  <p>
      Phone Number: (123)333-4444 <br>
      Email: thisisafakeemail@fake.com
  </p>

</div>
<div class = "rightone">
<figure>
      <!--image from https://emerj.com/ai-sector-overviews/ai-at-citi/-->
      <img src = "images/citibank.jpg" alt = "Citi Bank" width = 420/>
    </figure>
</div>
</div>
  </main>

  <?php include("includes/footer.php"); ?>
</body>

</html>
