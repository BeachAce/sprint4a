<?php include('includes/init.php');?>
<!DOCTYPE html>
<html lang="en">

<?php include('includes/header2.php'); ?>

<body>

<div class = "firstrow">

  <div class = "box2">
    <figure>
      <!--image from https://en.wikipedia.org/wiki/Citigroup-->
      <img src = "images/citi.png" alt = "pokeball symbol" width = 70/>
    </figure>
  </div>

  <div class = "box1">
    <h1>Investivia</h1>
  </div>
</div>

<hr/>
<div class = "topnav">
  <nav>
    <ul>
    <li class="<?php echo ($title == 'citi') ? 'current_page' : '' ?>"><a href="citi.php">Home</a></li>
      <li class="<?php echo ($title == 'videos') ? 'current_page' : '' ?>"><a href="videos.php">Lessons</a></li>
      <li class="<?php echo ($title == 'dictionary') ? 'current_page' : '' ?>"><a href="dictionary.php">Dictionary</a></li>
      <li class="<?php echo ($title == 'my_money') ? 'current_page' : '' ?>"><a href="portfolio.php">My Money</a></li>
    </ul>
  </nav>
</div>
<hr/>

  <main>

  <h2>Lessons</h2>
<ul>
<li><a href= "lesson1.php">Lesson 1</a></li>
<li><a href= "lesson2.php">Lesson 2</a></li>
<li><a href= "lesson3php">Lesson 3</a></li>
<li><a href= "lesson4.php">Lesson 4</a></li>
<li><a href= "lesson5.php">Lesson 5</a></li>
<li><a href= "lesson6.php">Lesson 6</a></li>
<li><a href= "lesson7.php">Lesson 7</a></li>
<li><a href= "lesson8.php">Lesson 8</a></li>
<li><a href= "lesson9.php">Lesson 9</a></li>
<li><a href= "lesson10.php">Lesson 10</a></li>
<li><a href= "lesson11.php">Lesson 11</a></li>
</ul>

  </main>
</body>

</html>
