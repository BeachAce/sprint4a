<?php include('includes/init.php');

if (isset($_GET['search'])) {
  $do_search = TRUE;
  $search = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);
  $search = trim($search);

}
?>

<!DOCTYPE html>
<html lang="en">

<?php include('includes/header2.php'); ?>

<body>

<div class = "firstrow">

  <div class = "box2">
    <figure>
      <!--image from https://en.wikipedia.org/wiki/Citigroup-->
      <img src = "images/citi.png" alt = "pokeball symbol" width = 70/>
    </figure>
  </div>

  <div class = "box1">
    <h1>Investivia</h1>
  </div>
</div>

<hr/>
<div class = "topnav">
  <nav>
    <ul>
    <li class="<?php echo ($title == 'citi') ? 'current_page' : '' ?>"><a href="citi.php">Home</a></li>
      <li class="<?php echo ($title == 'videos') ? 'current_page' : '' ?>"><a href="videos.php">Lessons</a></li>
      <li class="<?php echo ($title == 'dictionary') ? 'current_page' : '' ?>"><a href="dictionary.php">Dictionary</a></li>
      <li class="<?php echo ($title == 'my_money') ? 'current_page' : '' ?>"><a href="portfolio.php">My Money</a></li>
      <li class="<?php echo ($title == 'quiz') ? 'current_page' : '' ?>"><a href="quiz.php">Quiz</a></li>
    </ul>
  </nav>
</div>

<hr/>
  <main>

  <h2>Vocabulary Terms</h2>

  <?php
if ($do_search == FALSE){?>
  <form class = "form1" id="searchForm" action="dictionary.php" method="get" novalidate>

    <input type="text" name="search" required />
    <button type="submit">Search</button>
  </form>
  <?php }

 if ($do_search == TRUE){?>
 <form id="go_back" action="dictionary.php?" form method = "POST" >
<div class="go_back">
  <span></span>
  <button name="go_back" type="submit">Clear Search</button>
</div>
</form>
<?php
 }
?>

<?php

if ($do_search == FALSE){
$all_types = exec_sql_query($db, "SELECT term_name, term_description FROM dictionary1")->fetchAll(PDO::FETCH_ASSOC);
}

if ($do_search == TRUE){
    $all_types = exec_sql_query($db, "SELECT term_name, term_description FROM dictionary1 WHERE term_name LIKE '%' || :search || '%' ;", array(':search'=> $search))->fetchAll(PDO::FETCH_ASSOC);
}
?>

<div class = "types">
<?php
if (count($all_types) > 0) {
  foreach ($all_types as $type) {
      ?>
    <h3><?php echo $type["term_name"]; ?></h3>
    <p>Description: <?php echo $type["term_description"];?></p>
<?php
        }
      }

 ?>
 </div>


  </main>
</body>

</html>
