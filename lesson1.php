<?php include('includes/init.php');?>
<!DOCTYPE html>
<html lang="en">

<?php include('includes/header2.php'); ?>

<body>

<div class = "firstrow">

  <div class = "box2">
    <figure>
      <!--image from https://en.wikipedia.org/wiki/Citigroup-->
      <img src = "images/citi.png" alt = "pokeball symbol" width = 70/>
    </figure>
  </div>

  <div class = "box1">
    <h1>Investivia</h1>
  </div>
</div>

<hr/>
<div class = "topnav">
  <nav>
    <ul>
    <li class="<?php echo ($title == 'citi') ? 'current_page' : '' ?>"><a href="citi.php">Home</a></li>
      <li class="<?php echo ($title == 'videos') ? 'current_page' : '' ?>"><a href="videos.php">Lessons</a></li>
      <li class="<?php echo ($title == 'dictionary') ? 'current_page' : '' ?>"><a href="dictionary.php">Dictionary</a></li>
      <li class="<?php echo ($title == 'my_money') ? 'current_page' : '' ?>"><a href="portfolio.php">My Money</a></li>
    </ul>
  </nav>
</div>
<hr/>

  <main>

  <h2>Lesson 1</h2>

  <h3>Video</h3>

<iframe width="420" height="345" src="https://www.youtube.com/embed/yRr0_gJ-3mI">
</iframe>


  </main>
</body>

</html>
